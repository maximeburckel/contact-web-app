import { useEffect, useState } from 'react';
import { Outlet, useParams } from 'react-router-dom';
import { get_addresses, post_address, delete_address, update_address } from '../api/address';
import EditableText from './EditableText';
import ModifyComboBox from './ModifyComboBox';
import Phones from './PersonPhones';



// Addresses
// ========

const Addresses = () => {
    
    const [addresses, setAddresses] = useState([]);
    let {id} = useParams();
    const [loading, setLoad] = useState(true);

    const addAddress = (address) => {
    setAddresses(addresses => [address, ...addresses]);
    };

    const removeAddress = (index) => {
    setAddresses(addresses => addresses.filter((address) => address.id !== index));
    };

    const onUpdate = (index, address, city, country, label) => {
        const changes = {address: address, city: city, country: country, label : label};
        console.log(changes)
        update_address(id, index, address, city, country, label)
        .then(() => {
            const newAddress = addresses.map(address => {
                if (address.id === index) {
                    return {...address, ...changes};
                }
                return address;
            });
            return setAddresses(newAddress);
        });
    }

    

    useEffect(() => {
        setLoad(true)
        get_addresses(id).then((data) => setAddresses(data));
        setLoad(false)
    }, [id]);

    return <>
            <h3>Addresses postal : </h3>
            <AddAddress id={id} add={addAddress}/>
            <AddresslList
                id={id}
                addresses={addresses}
                remove={removeAddress}
                update = {onUpdate}
                loading={loading}
            />
    </>;
};


// AddressList
// =========

const AddresslList = ({id, addresses, remove, update,loading }) => {
    const [research, setResearch] = useState("");
    const handleSubmit = (e) => {
        e.preventDefault();
        setResearch(e.target.label.value);
      };

    return(
    <div id="container">
        <div>
            <AdressFilter handleSubmit={handleSubmit}/>
            <ul>
                {loading? <p>Loading...</p> :
                addresses.filter((address) => 
                address.label.includes(research.toLowerCase()))
                .map((address, index) => (
                <li key={address.id}> 
                    Adresse : <EditableText
                        text={address.address}
                        onUpdate={(res) => update(address.id, res, address.city, address.country, address.label)}
                    />
                    <br></br>
                    Ville : <EditableText
                        text={address.city}
                        onUpdate={(city) => update(address.id, address.address, city, address.country, address.label)}
                    />
                    <br></br>
                    Pays : <EditableText
                        text={address.country}
                        onUpdate={(country) => update(address.id, address.address, address.city, country, address.label)}
                    />
                    <br/>  
                    Label : <ModifyComboBox
                    text={address.label}
                    onUpdate={(label) => update(address.id, address.address, address.city, address.country, label)}
                    /> 
                    <br/>
                    <RemoveAddress
                        idPerso = {id}
                        idAddress = {address.id}
                        removeAddress={remove}
                    />
                </li>
                ))}
            </ul>
        </div>
        <div id="info">
            <Outlet />
        </div>
    </div>
    );
}

// AddAddress
// ===========

const AddAddress = ({id, add}) => {
    const handleSubmit = (e) => {
        e.preventDefault();
        post_address(id,{"address" : e.target.address.value, "city" : e.target.city.value, "country" : e.target.country.value, "label" : e.target.label.value})
        .then(data => add(data));
        e.target.address.value="";
        e.target.city.value="";
        e.target.country.value="";
      };

      

  return (
  <form onSubmit={handleSubmit}>
      <input type="text" name="address" />{' '}
      <input type="text" name="city" />{' '}
      <input type="text" name="country" />{' '}
      <select name="label">
          <option value="work">Work</option>
          <option value="home">Home</option>
      </select>
      <button>Add</button>
    </form>
  );
};


const AdressFilter = ({handleSubmit}) => {


    return (
        <form onSubmit={handleSubmit}>
            <select name="label">
                <option value="">Tout</option>
                <option value="work">Work</option>
                <option value="home">Home</option>
            </select>
            <button>Filter</button>
          </form>
        );

};

// RemoveAddress
// ===========

const RemoveAddress = ({idPerso, idAddress, removeAddress}) => {

    const del = (index) => {
        removeAddress(index);
        delete_address(idPerso, index);
    };

    return (
        <button className="small danger" onClick={() => { del(idAddress); }}>Supprimer</button>
    );
}

export default Addresses;
