import { json } from 'react-router-dom';
import React, { useState, useEffect } from 'react';
import { Link, NavLink, Outlet } from 'react-router-dom';
import './../api/group';
import {get_groups , remove_groups, create_group} from './../api/group.js';
import People from './people';



// Groups
// ========

const Groups = () => {
  const [groups, setGroups] = useState([]);
  const [loading, setLoading] = useState(true);
  const addGroup =  (payload) => {
    setGroups(groups => [
      ...groups, payload
    ]);
  }
  useEffect(() => {
    setLoading(true);
    get_groups().then((data) => {setGroups(data)});
    setLoading(false)
  }, []);


  const removeGroup = (id) => {
    remove_groups(id);
    setGroups(groups => groups.filter((group) => group.id !== id));
  }

  
  return<>
    <div id="container">
      <div id="sidebar">
        <AddGroupForm addGroup = {addGroup} groups={groups} setGroups={setGroups}/>
        <ul>
        <h3>Liste des groupes : </h3>
        <GroupsList groups={groups} removeGroup={removeGroup} loading={loading}/>
        </ul>
      </div>
      <div id="info">
        <Outlet />
      </div>
    </div>
  </>
}

const GroupsList = ({groups, removeGroup, loading}) => {

  const [search,setSearch] = useState("")

  const handleSearch = (e) => {
    e.preventDefault();
      setSearch(e.target.value)
  }

  return(<>
        <form>
          <label>Rechercher un Groupe: </label><input type="text" name="searching" onChange={handleSearch}/>
        </form>
    <ul>
        {loading ? <p>Loading...</p> : 
        groups.filter((group) => 
        group.title.toLowerCase().includes(search.toLowerCase()))
        .map((group) => (
            <li key={group.id}>
              <Link to={"/groups/"+group.id}>{group.title}</Link> 
              { <button className="small danger" onClick={() => removeGroup(group.id)}> X </button> }
            </li>
        ))}
    </ul>
    </>
  );
}

  
      
const AddGroupForm = ({ addGroup, groups, setGroups }) => {
    const handleSubmit = (e) => {
      e.preventDefault();
      create_group({"title":e.target.title.value}).then(data=> addGroup(data));
      e.target.title.value = '';
      
  };


    return (
      <form onSubmit={handleSubmit} >
        <input type="text" name="title" />{' '}
        <button>Add</button>
      </form>
    );
}



export default Groups;
