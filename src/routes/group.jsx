import React, { useEffect, useState } from 'react';
import { NavLink, useParams } from 'react-router-dom';
import { get_group_by_id, get_people_by_group_id, add_people_in_group, delete_person_from_group } from '../api/group';
import { get_people } from '../api/person';



export default function Group() {
  let { id } = useParams();
  const [group, setGroups] = useState({});
  const [loading, setLoading] = useState(true);

   useEffect(() => {
      setLoading(true)
      get_group_by_id(id).then((data) => setGroups(data));
      setLoading(false)
   }, [id]);


   if(loading){
    return (<p>Loading ...</p>)
  }
  else{ return (
  <>
          <h1>Nom du groupe : {group.title}</h1>
          <h2>Liste des membres : </h2>
          <PeopleList id={id} />
      </>
    )
  }

}


  function PeopleList({id}) {

    const [members, setMembers] = useState([]);
    const [allMembers, setAllMembers] = useState([]);
    const [loading, setLoading] = useState(true);

    const load = (id) => {
      setLoading(true);
      get_people_by_group_id(id).then((data) => setMembers(data));
      setLoading(false);
    }
   
    useEffect(() => {
        setLoading(true);
        get_people().then((data) => setAllMembers(data));
        setLoading(false)
    }, [id]);

    useEffect(() => {
        load(id);
    }, [id]);

    const addPerson =  (payload) => {  
        load(id);
        setMembers(members => [
          ...members, payload
        ]);
    }

    const removePerson = (person_id, id) => {
        setLoading(true)
        delete_person_from_group(person_id, id);
        setLoading(false)
        setMembers(members => members.filter((member) => member.id !== person_id));
      
   }

    const AddPeopleForm = ({ addPerson , groupId }) => {
        const handleSubmit = (e) => {
            setLoading(true)
          var idx = thelist.selectedIndex;
          var id = thelist.options[idx].value;
          e.preventDefault();
          add_people_in_group(id, groupId).then(data => addPerson(data)); 
          setLoading(false);
        };
        
        
        return (
          <form onSubmit={handleSubmit}>
            <label>Ajouter une personne à ce groupe</label>
            <select name="thelist" id="thelist">
            {allMembers.map((member) => (
                <option  key={member.id} value={member.id}>{member.firstname} {member.lastname}</option>
            )) }
            
            </select> 
            <button>Add</button>
          </form>
        );
      };

    


     return( <>
        <AddPeopleForm addPerson={addPerson} groupId={id}/>
     <ul>
        {loading ? <p>Loading...</p>
        :
        members.map((person) => (
          <li key={person.id}>
            <NavLink to={"/people/"+person.id}>{person.firstname} {person.lastname}</NavLink> 
            { <button className="small danger" onClick={() => removePerson(person.id,id)}> Supprimer </button> }
          </li>
        ))}
    </ul>
    </>)

}



  
  






