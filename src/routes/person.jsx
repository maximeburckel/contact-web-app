import { useEffect, useState } from 'react';
import { NavLink, useParams } from 'react-router-dom';
import Mails from './PersonMailAddresses';
import Address from './PersonPostalAddresses';
import Phone from './PersonPhones';
import { get_person, get_person_groups } from '../api/person';
import { delete_person_from_group } from '../api/group';


// Person
// ========

const Person = () => {
    
    let {id} = useParams();
    const [person, setPerson] = useState("");
    
    useEffect(() => {
        get_person(id).then((data) => setPerson(data))
    }, [id])
    
    
    return <>
    <h2>{person.firstname}  {person.lastname}</h2>
    <Group id={person.id} />
    <Mails id={person.id}/>
    <Address id={person.id}/>
    <Phone id={person.id}/>
    </>
};

const Group = () => {

    let {id} = useParams();
    const [groups, setGroups] = useState([]);
    const [loading, setLoading] = useState(true);
    
    useEffect(() => {
        setLoading(true)
        get_person_groups(id).then((data) => setGroups(data))
        setLoading(false)
    }, [id])

    const del = (group_id) => {
        delete_person_from_group(id,group_id)
        setGroups(groups => groups.filter((group) => group.id !== group_id));
    }

    return <>
        <p>Groups : </p>
        {loading? <p>Loading...</p>:
        groups.map((group, index) => (
        <div key={group.id}>
            <NavLink to={"/groups/"+group.id}> | {group.title} |</NavLink> 
            <button className="small danger" onClick={() => { del(group.id); }}>X</button>
        </div>
        ))}
    </>
}

export default Person;
