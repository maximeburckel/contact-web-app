import { Link, NavLink, Outlet } from 'react-router-dom';

export default function Root() {
  return (<>
    <header>
      <h2><Link to="/">W42</Link></h2>
      <ul>
        <li><NavLink to="/groups">Groups</NavLink></li>
        <li><NavLink to="/people">People</NavLink></li>
      </ul>
    </header>
    <main>
      <Outlet />
    </main>
    <footer>
      <em>Made with React & react-router</em>
    </footer>
  </>);
}
