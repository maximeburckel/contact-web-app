import { useEffect, useState } from 'react';
import { Outlet, NavLink } from 'react-router-dom';
import { post_people, get_people, delete_people } from '../api/person';


// PeopleApp
// ========

const PeopleApp = () => {

  const [loading, setLoad] = useState(true);
  const [people, setPeople] = useState([]);

  const addPerson = (person) => {
    setPeople(people => [person, ...people]);
  };

  const removePerson = (index) => {
    setPeople(people => people.filter((person) => person.id !== index));
  };

  useEffect(() => {
    setLoad(true)
    get_people().then((data) => setPeople(data));
    setLoad(false)
  }, []);

  
  
    return <>
    <h1>Liste des personnes : </h1>
      <PeopleList
        people={people}
        setPeople={setPeople}
        addPerson={addPerson}
        removePerson={removePerson}
        loading={loading}
      />
    </>;

};

// PeopleList
// =========

const PeopleList = ({ people, setPeople, addPerson, removePerson, loading}) => {
  const [search,setSearch] = useState("")

  const handleSearch = (e) => {
      e.preventDefault();
      setSearch(e.target.value)
    };

  return(
        <div id="container">
            <div id="sidebar">
                <AddPerson addPerson={addPerson} people={people} setPeople={setPeople}/>
                <form>
                <label>Rechercher un Prénom ou un Nom: </label><input type="text" name="searchingFirstname" onChange={handleSearch}/>
                </form>
                <ul>
                    {loading ? <p>Loading...</p> :
                    people.filter(person =>
                      {
                        const lastname = person.lastname.toLowerCase()
                        const firstname = person.firstname.toLowerCase()
                        return (lastname.includes(search.toLowerCase()) 
                        || firstname.includes(search.toLowerCase()))
                      })
                      .map((person) => (
                    <li key={person.id}> <NavLink to={"/people/"+person.id}>{person.firstname} {person.lastname}</NavLink>
                      <RemovePerson
                          id = {person.id}
                          remove={removePerson}
                      />
                    </li>
                    ))}
                </ul>
            </div>
            <div id="info">
                <Outlet />
            </div>
        </div>
  );
}

// AddPerson
// ===========

const AddPerson = ({addPerson, people, setPeople}) => {
    const handleSubmit = (e) => {
        e.preventDefault();
        post_people({"firstname" : e.target.firstname.value, "lastname" : e.target.lastname.value})
        .then(data => addPerson(data));
        e.target.firstname.value="";
        e.target.lastname.value="";
      };

      

  return (
  <form onSubmit={handleSubmit}>
      <label>Firstname : </label>
      <input type="text" id="firstname" name="firstname" />{' '}
      <br/>
      <label>Lastname : </label>
      <input type="text" id="lastname" name="lastname" />{' '}
      <br/>
      <button>Add</button>
    </form>
  );
};

// RemovePerson
// ============

const RemovePerson = ({id, remove}) => {

    const del = (index) => {
        remove(index);
        delete_people(index);
    };

    return (
        <button className="small danger" onClick={() => { del(id); }}>X</button>
    );
}

export default PeopleApp;


