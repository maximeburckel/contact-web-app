
import React, { useState, useEffect } from 'react';
import '../api/group';



export default function ModifyComboBox(props) {
    const [editing, setEditing] = useState(false);
    const [updating, setUpdating] = useState(false);
    const [text, setText] = useState(props.text);
    const handleEdit = () => {
        setText(props.text);
        setEditing(true);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        setUpdating(true);
        props.onUpdate(text);
        setEditing(false);
        setUpdating(false);
        
    }

    const handleChange = (e) => {
            console.log(e.target.value)
            setText(e.target.value);
        
    }



    
    
    if(editing){
        return(
        <form onSubmit={handleSubmit}>
            <select name="label" onChange={handleChange}>
                <option>Veuillez choisir un label</option>
                <option value="work">Work</option>
                <option value="home">Home</option>
            </select>
            <button type="submit" disabled={updating}>Valider</button>
        </form>)
    }
    else if(updating){
        
          return(<>
             {text}
            <button onClick={handleEdit}>Modifier</button>
            </>)
    }

    else{
        return(<>
            {props.text}
           <button onClick={handleEdit}>Modifier</button>
           </>)
    }


}

