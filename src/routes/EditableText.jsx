
import React, { useState, useEffect } from 'react';
import '../api/group';



export default function EditableText(props) {
    const [editing, setEditing] = useState(false);
    const [updating, setUpdating] = useState(false);
    const [text, setText] = useState(props.text);
    const handleEdit = () => {
        setText(props.text);
        setEditing(true);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        setUpdating(true);
        props.onUpdate(text);
        setEditing(false);
        setUpdating(false);
        
    }

    const handleChange = (e) => {
        
            setText(e.target.value);
        
    }



    
    
    if(editing){
        return(
        <form onSubmit={handleSubmit}>
            <input type="text" value={text} onChange={handleChange}/>
            <button type="submit" disabled={updating}>Valider</button>
        </form>)
    }
    else if(updating){
        
          return(<>
             {text} <button onClick={handleEdit}>Modifier</button>
            </>)
    }

    else{
        return(<>
            {props.text} <button onClick={handleEdit}>Modifier</button>
           </>)
    }


}

