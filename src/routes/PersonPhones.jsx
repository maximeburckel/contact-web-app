import { useEffect, useState } from 'react';
import { Outlet, useParams } from 'react-router-dom';
import { get_phones, post_phone, delete_phone, update_phone } from '../api/phone';
import EditableText from './EditableText';
import ModifyComboBox from './ModifyComboBox';


// Phones
// ========

const Phones = () => {
    
    const [phones, setPhones] = useState([]);
    let {id} = useParams();
    const [loading, setLoad] = useState(true);

    const addPhone = (phone) => {
    setPhones(phones => [phone, ...phones]);
    };

    const removePhone = (index) => {
    setPhones(phones => phones.filter((phone) => phone.id !== index));
    };

    const onUpdate = (index, number, label) => {
        const changes = {number: number, label: label};
        console.log("change",label)
        update_phone(id, index, number, label)
        .then(() => {
            const newPhones = phones.map(phone => {
                if (phone.id === index) {
                   // console.log(changes)
                    return {...phone, ...changes};
                }
                console.log(phone)
                return phone;

            });
            return setPhones(newPhones);
        });
    }


    useEffect(() => {
        setLoad(true)
        get_phones(id).then((data) => setPhones(data));
        setLoad(false)
    }, [id]);

    return <>
        <h3>Phones : </h3>
        <AddPhone id={id} add={addPhone}/>
        <PhonelList
            id={id}
            phones={phones}
            remove={removePhone}
            update={onUpdate}
            loading={loading}
        />
    </>;
};


// PhoneList
// =========

const PhonelList = ({id, phones, remove, update, loading}) => {
    const [research, setResearch] = useState("");
    const handleSubmit = (e) => {
        e.preventDefault();
        // if(e.target.value !== "")
            setResearch(e.target.label.value);
      };
      
    return(
    <div id="container">
        <div>
            <PhoneFilter handleSubmit={handleSubmit}/>
            <ul>
                {loading?<p>Loading...</p> :
                phones.filter((phone) => 
                phone.label.includes(research.toLowerCase()))
                .map((phone, index) => (
                <li key={phone.id}> 
                    Numéros : <EditableText
                                    text={phone.number}
                                    onUpdate={(number) => update(phone.id, number, phone.label)}
                        /> 
                    <br/>
                    Label : <ModifyComboBox text={phone.label} 
                        onUpdate={(label) => update(phone.id, phone.number, label)}
                        /> 
                    <br/> 
                    <RemovePhone
                            idPerso = {id}
                            idPhone = {phone.id}
                            removePhone={remove}
                    />
                    

                    
                </li>
                ))}
            </ul>
        </div>
        <div id="info">
            <Outlet />
        </div>
    </div>
    );
}

// AddPhone
// ===========

const AddPhone = ({id, add}) => {
    const handleSubmit = (e) => {
        e.preventDefault();
        post_phone(id, {"number" : e.target.number.value, "label" : e.target.label.value})
        .then(data => add(data));
        e.target.number.value = "";
      };

  return (
  <form onSubmit={handleSubmit}>
      <input type="tel" name="number" />{' '}
      <select name="label">
          <option value="work">Work</option>
          <option value="home">Home</option>
      </select>
      <button>Add</button>
    </form>
  );
};


//PhoneFilter
//===========
const PhoneFilter = ({handleSubmit}) => {


    return (
        <form onSubmit={handleSubmit}>
            <select name="label">
                <option value="">Tout</option>
                <option value="work">Work</option>
                <option value="home">Home</option>
            </select>
            <button>Filter</button>
          </form>
        );

};

// RemovePhone
// ===========

const RemovePhone = ({idPerso, idPhone, removePhone}) => {

    const del = (index) => {
        removePhone(index);
        delete_phone(idPerso, index);
    };

    return (
        <button className="small danger" onClick={() => { del(idPhone); }}>Supprimer</button>
    );
}

export default Phones;
