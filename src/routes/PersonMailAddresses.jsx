import { useEffect, useState } from 'react';
import { Outlet, useParams } from 'react-router-dom';
import { get_mails, post_mail, delete_mail, update_mail } from '../api/mail';
import EditableText from './EditableText';
import ModifyComboBox from './ModifyComboBox';


// Mails
// ========

const Mails = () => {
    
    const [mails, setMails] = useState([]);
    let {id} = useParams();
    const [loading, setLoad] = useState(true);

    const addMail = (mail) => {
    setMails(mails => [mail, ...mails]);
    };

    const removeMail = (index) => {
    setMails(mails => mails.filter((mail) => mail.id !== index));
    };

    const onUpdate = (index, address, label) => {
        const changes = {address: address, label: label};
        update_mail(id, index, address, label)
        .then(() => {
            const newMail = mails.map(mail => {
                if (mail.id === index) {
                    return {...mail, ...changes};
                }
                return mail;
            });
            return setMails(newMail);
        });
    }

    useEffect(() => {
        setLoad(true)
        get_mails(id).then((data) => setMails(data));
        setLoad(false)
    }, [id]);

    return <>
        <h3>Mails Addresses : </h3>
        <AddMail id={id} add={addMail}/>
        <MaillList
            id={id}
            mails={mails}
            remove={removeMail}
            update={onUpdate}
            loading={loading}
        />
    </>;
};


// MailList
// =========

const MaillList = ({id, mails, remove, update, loading}) => 
{   
    const [research, setResearch] = useState("");
    const handleSubmit = (e) => {
        e.preventDefault();
        setResearch(e.target.label.value);
      };
   
     
    
    return(
    
        <div id="container">
            <div>
                <MailFilter handleSubmit={handleSubmit} />
                <ul>
                    {loading? <p>Loading...</p>
                    :
                    
                    mails.filter((mail) => 
                    mail.label.includes(research.toLowerCase()))
                    .map((mail, index) => (
                    <li key={mail.id}> 
                        Adresse mail : <EditableText
                        text={mail.address}
                        onUpdate={(address) => update(mail.id, address, mail.label)}
                        /> 
                        <br/>
                        Label : <ModifyComboBox
                        text={mail.label}
                        onUpdate={(label) => update(mail.id, mail.address, label)}
                        />  
                        <br/>   
                        <RemoveMail
                            idPerso = {id}
                            idMail = {mail.id}
                            removeMail={remove}
                        />
                        
                    </li>
                    ))}
                    
                </ul>
            </div>
            <div id="info">
                <Outlet />
            </div>
        </div>
    );
}

// AddMail
// ===========

const AddMail = ({id, add}) => {
    const handleSubmit = (e) => {
        e.preventDefault();
        post_mail(id, {"address" : e.target.address.value, "label" : e.target.label.value})
        .then(data => add(data));
        e.target.address.value ="";
      };

  return (
  <form onSubmit={handleSubmit}>
      <input type="email" name="address" />{' '}
      <select name="label">
          <option value="work">Work</option>
          <option value="home">Home</option>
      </select>
      <button>Add</button>
    </form>
  );
};

//MailFilter
//=========
const MailFilter = ({handleSubmit}) => {

   
    return (
        <form onSubmit={handleSubmit}>
            <select name="label">
                <option value="">Tout</option>
                <option value="work">Work</option>
                <option value="home">Home</option>
            </select>
            <button>Filter</button>
          </form>
        );

};

// RemoveMail
// ===========

const RemoveMail = ({ idPerso, idMail, removeMail}) => {

    const del = (index) => {
        removeMail(index);
        delete_mail(idPerso, index);
    };

    return (
        <button className="small danger" onClick={() => { del(idMail); }}>Supprimer </button>
    );
}

export default Mails;
