import React from 'react';
import ReactDOM from 'react-dom/client';
import {
  createBrowserRouter,
  RouterProvider,
  Navigate
} from 'react-router-dom';

import './index.css';

import Root from './routes/root';
import PeopleApp from './routes/people';
import Groups from './routes/groups';
import Person from './routes/person';
import Group from './routes/group';

const router = createBrowserRouter([
  {
    path: '/',
    element: <Root />,
    children: [
      {
        index: true,
        element: <>
          <h3>Bienvenue :-)</h3>
          <p>Sélectionner un groups ou people</p>
        </>
      },
      {
        path: 'people',
        element: <PeopleApp />,
        children: [
          {
            index: true,
            element: <p>Please select a person</p>
          },
          {
            path: ':id',
            element: <Person />
          }
        ]
      },
      {
        path: 'groups',
        element: <Groups />,
        children: [
          {
            index: true,
            element: <p>Please select a group</p>
          },
          {
            path: ':id',
            element: <Group/>
          }
        ]
      }
    ]
  },
  {
    path: '*',
    element: <Navigate to="/" replace />
  }
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
);
