import {url} from './config';

// get_mails
// =========

export const get_mails = (id) => {
	return fetch(url + 'person/'+ id + '/mailAddress' ).then((res) => res.json());
};

// post_mail
// =========

export const post_mail = (id, body) => {
    return fetch(url + 'person/' + id + '/mailAddress', {method: "POST",body: JSON.stringify(body), headers: {"Content-Type": "application/json"}}).then(res => res.json());
};

// delete_mail
// =========

export const delete_mail = (idPerson, idMail) => {
    return fetch(url + 'person/' + idPerson + '/mailAddress/' + idMail, {method: "DELETE"}).then();
};

export const update_mail = (idPerson, idMail, address, label) => {
    return fetch(url + 'person/' + idPerson + '/mailAddress/' + idMail, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ address, label })
    })
    .then(res => res.json());
}
