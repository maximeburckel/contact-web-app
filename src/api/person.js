import {url} from './config';

// get_person
// =========

export const get_person = (id) => {
	return fetch(url + 'person/' + id).then((res) => res.json());
};

// get_people
// =========

export const get_people = () => {
	return fetch(url + 'person').then((res) => res.json());
};

// post_people
// =========

export const post_people = (body) => {
    return fetch(url + 'person', {method: "POST",body: JSON.stringify(body), headers: {"Content-Type": "application/json"}}).then(res => res.json());
};

// delete_people
// =========

export const delete_people = (index) => {
    return fetch(url + 'person/'+index, {method: "DELETE"}).then();
};

// get_person_groups
// =========

export const get_person_groups = (index) => {
    return fetch(url + 'person/'+index+'/groups', {method: "GET"}).then((res) => res.json());
};