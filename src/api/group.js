import { url } from "./config";
   
   export const  get_groups = () => {
        return fetch(url + 'group')
        .then((res) => res.json());
    };

    export const create_group = (payload) => {
      return fetch(url + "group", {
        method: 'POST',
        headers : {
           "Content-Type": 'application/json'
        },
        body : JSON.stringify(payload)
      })
      .then(res => {return res.json()})
    };

    export const delete_person_from_group = (person_id, group_id) => {
        fetch(url+'person/'+person_id+'/group/'+group_id, {method:'DELETE'})
    }

    export const remove_groups = (id) => {
        fetch(url + "group/"+id,{
        method: 'DELETE',
      })
    };

     export const get_group_by_id = (id) => {
        return fetch(url+'group/'+id)
        .then(res => {return res.json()})
      }

      export const get_people_by_group_id = (id) => {
        return fetch(url+'group/'+id+'/people').then(res => res.json())
      }

      export const add_people_in_group = (person_id, group_id) => {
        return fetch(url+"person/"+person_id+"/group/"+group_id, {method: 'POST'})
      }



