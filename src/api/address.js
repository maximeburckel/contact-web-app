import {url} from './config';

// get_addresses
// =========

export const get_addresses = (id) => {
	return fetch(url + 'person/'+ id + '/postalAddress' ).then((res) => res.json());
};

// post_address
// =========

export const post_address = (id, body) => {
    return fetch(url + 'person/' + id + '/postalAddress', {method: "POST",body: JSON.stringify(body), headers: {"Content-Type": "application/json"}}).then(res => res.json());
};

// delete_address
// =========

export const delete_address = (idPerson, idMail) => {
    return fetch(url + 'person/' + idPerson + '/postalAddress/' + idMail, {method: "DELETE"}).then();
};

export const update_address = (idPerson, idMail, address, city, country, label) => {
    return fetch(url + 'person/' + idPerson + '/postalAddress/' + idMail, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ address ,city ,country,label })
    })
    .then(res => res.json());
}