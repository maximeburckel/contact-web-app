import {url} from './config';

// get_phones
// =========

export const get_phones = (id) => {
	return fetch(url + 'person/'+ id + '/phone' ).then((res) => res.json());
};

// post_phone
// =========

export const post_phone = (id, body) => {
    return fetch(url + 'person/' + id + '/phone', {method: "POST",body: JSON.stringify(body), headers: {"Content-Type": "application/json"}}).then(res => res.json());
};

// delete_phone
// =========

export const delete_phone = (idPerson, idMail) => {
    return fetch(url + 'person/' + idPerson + '/phone/' + idMail, {method: "DELETE"}).then();
};

//update phone
export const update_phone = (person_id, phone_id, number, label) => {
    return fetch(url + 'person/' + person_id + '/phone/' + phone_id, {
        method: 'PUT',
        body: JSON.stringify({ number, label }),
        headers: {
            'Content-Type': 'application/json'
        },
    })
    .then(res => {
        res.json()
    });

}